// console.log("welcome");

//fetch("https://jsonplaceholder.typicode.com/todos")
//.then(res => console.log(res.status));

// async function fetchData(){
// 	let result = await(fetch("https://jsonplaceholder.typicode.com/todos"))
// 	//console.log(result);

// 	let json = await result.json();
// 	console.log(json);
// 	//console.log("hello world");
// }

// fetchData();







fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json())
.then(response => console.log(response))

console.log("The item 'delectus aut autem' on the list has a status of false");




fetch("https://jsonplaceholder.typicode.com/todos ",{
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Created To Do List Item",
		userId:1
	})
})
.then(res => res.json())
.then(response => console.log(response))





// fetch("https://jsonplaceholder.typicode.com/todos	",
// {
// 	method: "POST",
// 	headers: {
// 		"Content-Type" : "application/json"
// 	},
// 	body: JSON.stringify({
// 		title: "Created To Do List Item",
// 		userId:1
// 	})

// })

// .then(response => response.json())
// .then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1 ",{

	method : "PUT",
	headers : {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		DateCompleted: "Pending",
		description: "To update the my todo list with a different data structure",
		status: "Pending",
		title: "Updated To Do List Item",
		userId:1
	})

})
.then(res => res.json())
.then(response => console.log(response))


 








// S33 Activity:
// 1. In the S28 folder, create an activity folder and an index.html and a script.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID
// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
// 13. Create a git repository named S28.
// 14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 15. Add the link in Boodle.